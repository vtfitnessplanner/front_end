# Gobbleup Frontend

## Application Hosting

### Creating a Dev Container
```
# Create the image
docker build -t gobbleup-frontend:dev1 .
# Run the container locally: (Access on a browser http://localhost:8080)
docker run --name gobbleup-frontend -d -p 8080:80 gobbleup-frontend:dev1
```

### Pushing to Production Steps

#### Prerequisite steps:
1. Install docker CLI in your environment
2. Setup kubectl and connect to Kubernetes cluster: Info is here: https://discord.com/channels/1197299002757087363/1200544483155124404/1200545167573258281
3. Create a Dockerhub account on dockerhub.com
4. `docker login` in your environment.

#### Build the Image
Replace dwhassl with your Docker username. This is just my example.
```
# Create the prod image.
docker build -t dwhassl/gobbleup-frontend:prod1 .

# Push to Dockerhub
docker push dwhassl/gobbleup-frontend:prod1 
```
#### Apply to Kubernetes
Apply the Kubernetes resources after pushing to Dockerhub
```
#IMPORTANT: Replace the image in the ./kubernetes/deployment.yaml with the image you just pushed.

kubectl apply -f ./kubernetes/deployment.yaml
```

#### Accessing the App
```
# ingress created a route to publicly accessible domain:

https://gobbleup.discovery.cs.vt.edu
```

#### Front End Low Fidelity Video Demo
```
https://www.youtube.com/watch?v=hIRGWkJ6-Hg&feature=youtu.be

```
