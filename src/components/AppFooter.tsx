import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFacebook, faInstagram} from "@fortawesome/free-brands-svg-icons";
import '../assets/css/AppFooter.css'

function AppFooter(){
    return(
        <footer className="container">
            <p className="copyright">Copyright 2023 © Byte Sized Books LLC</p>
            <section className="links">
                <Link to="/">about</Link>
                <Link to="/">contact</Link>
                <Link to="/">directions</Link>
            </section>
            <section className="social-media-icons">
                <Link to="/" className="button">
                    <FontAwesomeIcon icon={faInstagram} />
                </Link>
                <Link to="/" className="button">
                    <i className="fab fa-facebook"></i>
                    <FontAwesomeIcon icon={faFacebook} />
                </Link>
            </section>
        </footer>
    )
}
export default AppFooter;
