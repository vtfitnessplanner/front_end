import React, { useState, useEffect } from 'react';
import Meal from './Meal';
// import AddFood from './AddFood'; // Assume this component will be created
import '../assets/css/Dashboard.css';
import { useNavigate } from "react-router-dom";
import {auth} from "./FireBase";
import axios from "axios";

interface FoodItem {
    mealId?: number;
    foodId: number;
    foodName: string;
    foodCalorie: number;
    foodFat: number;
    foodCholesterol: number;
    foodSodium: number;
    foodCarb: number;
    foodSugar: number;
    foodProtein: number;
    foodServingTime: string;
    foodLocation: string;
}

interface CurrentMeal {
    name: string;
    date: Date;
}


function Dashboard() {
    // State for the selected date
    const [selectedDate, setSelectedDate] = useState(new Date());
    const [mealModalVisible, setMealModalVisible] = useState(false);
    const [currentMeal, setCurrentMeal] = useState({ name: '', date: new Date() });
    const [mealItems, setMealItems] = useState([]);
    const [mealHistory, setMealHistory] = useState([]);
    const navigate = useNavigate();
    // Totals for the day (should be calculated based on mealItems or fetched from backend)
    const [totalCalories, setTotalCalories] = useState(0);
    const [totalProtein, setTotalProtein] = useState(0);
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const [authToken, setAuthToken] = useState(null);

    useEffect(() => {
        const isLoggedIn = localStorage.getItem('isLoggedIn');
        if (isLoggedIn !== 'true' || isLoggedIn === null) {
            navigate('/login');
        }
    }, [navigate]);

    useEffect(() => {
        const fetchToken = async () => {
            if (auth.currentUser) {
                try {
                    const token = await auth.currentUser.getIdToken();
                    // @ts-ignore
                    setAuthToken(token);
                    setIsAuthenticated(true);
                } catch (error) {
                    console.error('Error fetching token:', error);
                    setIsAuthenticated(false);
                }
            } else {
                setIsAuthenticated(false);
            }
        };

        fetchToken();
    }, [auth.currentUser]); // Reacting to changes in the currentUser object

    useEffect(() => {
        if (authToken && isAuthenticated) {
            fetchMealHistory(selectedDate);
        } else {
            console.log('No token available');
        }
    }, [authToken, selectedDate]); // Dependent on authToken and selectedDate





    useEffect(() => {
        // @ts-ignore
        const newTotalCalories = mealItems.reduce((acc, item) => acc + (item.foodCalorie * item.quantity), 0);
        // @ts-ignore
        const newTotalProtein = mealItems.reduce((acc, item) => acc + (item.foodProtein * item.quantity), 0);

        setTotalCalories(newTotalCalories);
        setTotalProtein(newTotalProtein);
    }, [mealItems]);


    const handleAddFoodItem = async (item: FoodItem, date: Date) => {
        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const day = String(date.getDate()).padStart(2, '0');
        const formattedDate = `${year}-${month}-${day}`;
        console.log('Formatted date:', formattedDate);
        const url = `https://backend-gobbleup.discovery.cs.vt.edu/meals/history/`;
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'X-Firebase-AppCheck': authToken,
            }
        };
        const data = {
            foodDate: formattedDate,
            foodID: item.foodId,
        }
        try {
            const response = await axios.post(url, data, config);
            console.log('Added item:', response.data);
            const updatedHistory = [...mealHistory, item]; // This assumes the add was successful.
            // @ts-ignore
            setMealHistory(updatedHistory); // Update the meal history state
        } catch (error) {
            console.error('Error adding item:', error);
        }
    };

    const handleRemoveFoodItem = async (item: FoodItem) => {
        const url = `https://backend-gobbleup.discovery.cs.vt.edu/meals/history/`;
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'X-Firebase-AppCheck': authToken,
            },
            params: {
                foodId: item.mealId,
            }
        };
        try {
            const response = await axios.delete(url, config);
            console.log('Removed item:', response.data);
            const updatedHistory = mealHistory.filter((historyItem: FoodItem) => historyItem.foodId !== item.foodId);
            setMealHistory(updatedHistory);
        } catch (error) {
            console.error('Error removing item:', error);
        }
    }

    // @ts-ignore
    const fetchMealHistory = async (date) => {
        try {
            const year = date.getFullYear();
            const month = String(date.getMonth() + 1).padStart(2, '0');
            const day = String(date.getDate()).padStart(2, '0');
            const formattedDate = `${year}${month}${day}`;
            console.log('Formatted date:', formattedDate);
            const url = `https://backend-gobbleup.discovery.cs.vt.edu/meals/history/`;
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Firebase-AppCheck': authToken,
                },
                params: {
                    date: formattedDate,
                }
            }
            const response = await axios.get(url, config);
            console.log('Meal history:', response.data.meal_item);
            if (response.data.meal_item.length === 0) {
                console.log('No meal history found');
                return;
            }
            const mealData = response.data.meal_items.map((mealItem: any) => ({
                mealId: mealItem.mealId, // Add this line
                ...mealItem.foodItem,
                quantity: 1, // Default quantity
            }));
            setMealHistory(mealData);
        } catch (error) {
            console.error('Error fetching meal history:', error);
        }
    };


    const openMealModal = (mealName: string) => {
        setCurrentMeal({
            name: mealName,
            date: selectedDate
        });
        setMealModalVisible(true);
    };

    const closeMealModal = () => {
        setMealModalVisible(false);
    };

    return (
        <div className="dashboard">
            <div className="date-navigator">
                <button onClick={() => setSelectedDate(new Date(selectedDate.setDate(selectedDate.getDate() - 1)))}>Previous</button>
                <span>{selectedDate.toDateString()}</span>
                <button onClick={() => setSelectedDate(new Date(selectedDate.setDate(selectedDate.getDate() + 1)))}>Next</button>
            </div>

            <div className="meals">
                <button onClick={() => openMealModal('Breakfast')}>Breakfast</button>
                <button onClick={() => openMealModal('Lunch')}>Lunch</button>
                <button onClick={() => openMealModal('Dinner')}>Dinner</button>
            </div>

            {mealModalVisible && (
                <Meal
                    mealName={currentMeal.name}
                    mealDate={currentMeal.date}
                    items={mealItems}
                    onClose={closeMealModal}
                    mealHistory={mealHistory}
                    onAddItem={handleAddFoodItem}
                    onRemoveItem={handleRemoveFoodItem}
                />
            )}

        </div>
    );
}

export default Dashboard;
