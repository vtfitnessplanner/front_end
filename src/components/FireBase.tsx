import { initializeApp } from 'firebase/app'
import {getAuth} from 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyBftyMcUBq24_kLocMNXW1ZH_nkX29Gqpo",
    authDomain: "gobbleup-e3e1a.firebaseapp.com",
    projectId: "gobbleup-e3e1a",
    storageBucket: "gobbleup-e3e1a.appspot.com",
    messagingSenderId: "920530204593",
    appId: "1:920530204593:web:c879e01896b074aab9af90",
    measurementId: "G-4H5V2XLHZ9"
};

const app = initializeApp(firebaseConfig)
const auth = getAuth(app)

export {auth}
