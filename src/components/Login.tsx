import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth } from './FireBase';
import { useNavigate } from 'react-router-dom';
import '../assets/css/Login.css';

function Login() {
    const navigate = useNavigate();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [notice, setNotice] = useState('');

    useEffect(() => {
        const isLoggedIn = localStorage.getItem('isLoggedIn');
        if (isLoggedIn === 'true') {
            navigate('/dashboard');
        }
    }, [navigate]);
    const loginWithUsernameAndPassword = async (e: { preventDefault: () => void; }) => {
        e.preventDefault();

        try {
            await signInWithEmailAndPassword(auth, email, password);
            localStorage.setItem('isLoggedIn', 'true');
            localStorage.setItem('userEmail', email);
            navigate("../profile");
            window.location.reload();
        } catch {
            setNotice("You entered a wrong username or password.");
        }
    };

    return (
        <div className="login-container">
            <form className="login-form">
                {notice !== '' &&
                    <div role="alert" className="alert">
                        {notice}
                    </div>
                }
                <div>
                    <label htmlFor="exampleInputEmail1">Email address</label>
                    <input
                        type="email"
                        id="exampleInputEmail1"
                        placeholder="name@example.com"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        className="form-control"
                    />
                </div>
                <div>
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input
                        type="password"
                        id="exampleInputPassword1"
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        className="form-control"
                    />
                </div>
                <div>
                    <button type="submit" onClick={loginWithUsernameAndPassword} className="submit-button">
                        Submit
                    </button>
                </div>
                <div className="signup-prompt">
                    <span>Need to sign up for an account? <Link to="../register">Click here.</Link></span>
                </div>
            </form>
        </div>
    );
}

export default Login;
