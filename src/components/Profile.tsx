import { useNavigate } from "react-router-dom";
import { auth } from "./FireBase";
import { signOut } from "firebase/auth";
import { useEffect, useState } from 'react';
import {useAuth} from "../Contexts/AuthContext";

const Profile = () => {
    const navigate = useNavigate();
    const [userEmail, setUserEmail] = useState('');
    const {handleLogout} = useAuth();

    useEffect(() => {
        const isLoggedIn = localStorage.getItem('isLoggedIn');
        console.log(isLoggedIn)
        if (isLoggedIn !== 'true' || isLoggedIn === null) {
            navigate('/login');
        }
    }, [navigate]);

    useEffect(() => {
        // Retrieve user's email from local storage
        const email = localStorage.getItem('userEmail');
        if (email) {
            setUserEmail(email);
        }
    }, []);

    // @ts-ignore

    const logoutUser = async (e) => {
        e.preventDefault();
        console.log('Logging out')
        handleLogout();
        await signOut(auth);
        // Clear user's email and login state from local storage on logout
        navigate("/");
        window.location.reload();
    }

    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-4 text-center">
                    {/* Display the email from local storage */}
                    <p>Welcome <em className="text-decoration-underline">{userEmail}</em> to Gobble Up. You are logged in!</p>
                    <div className="d-grid gap-2">
                        <button type="submit" className="btn btn-primary pt-3 pb-3" onClick={(e) => logoutUser(e)}>Logout</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Profile;
