import React, { useState } from "react";
import axios from 'axios';
import { auth } from "./FireBase";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { Link, useNavigate } from "react-router-dom";
import '../assets/css/Register.css';

const Register = () => {
    const navigate = useNavigate();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [dob, setDOB] = useState(""); // Already formatted as 'YYYY-MM-DD' by the input type='date'
    const [gender, setGender] = useState("");
    const [notice, setNotice] = useState("");

    // @ts-ignore
    const validateEmail = (email) => /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);

    const validateForm = () => {
        if (!validateEmail(email)) {
            setNotice("Please enter a valid email address.");
            return false;
        }
        if (password.length < 5) {
            setNotice("Password must be at least 5 characters long.");
            return false;
        }
        if (password !== confirmPassword) {
            setNotice("Passwords do not match.");
            return false;
        }
        if (!gender) {
            setNotice("Please select a gender.");
            return false;
        }
        if (!dob) {
            setNotice("Please enter your date of birth.");
            return false;
        }
        const today = new Date();
        const birthDate = new Date(dob);
        let age = today.getFullYear() - birthDate.getFullYear();
        const m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        if (age < 18) {
            setNotice("You must be at least 18 years old.");
            return false;
        }
        return true;
    };

    // @ts-ignore
    const registerWithUsernameAndPassword = async (e) => {
        e.preventDefault();
        if (!validateForm()) return; // Stop the registration if validation fails

        try {
            const userCredential = await createUserWithEmailAndPassword(auth, email, password);
            const token = await userCredential.user.getIdToken(); // Get the Firebase ID token for the authenticated user

            await axios.post('https://backend-gobbleup.discovery.cs.vt.edu/auth/register', {
                fname: firstName,
                lname: lastName,
                DOB: dob,  // Passed directly in the 'YYYY-MM-DD' format
                gender: gender,
            }, {
                headers: {
                    'X-Firebase-AppCheck': token,
                    'Content-Type': 'application/json'
                }
            });
            navigate("/");
        } catch (error) {
            setNotice("Sorry, something went wrong. Please try again.");
        }
    };

    return (
        <div className="register-container">
            <form className="register-form" onSubmit={registerWithUsernameAndPassword}>
                {notice !== '' && (
                    <div role="alert" className="alert">
                        {notice}
                    </div>
                )}
                <div>
                    <label htmlFor="fname">First Name</label>
                    <input type="text" id="fname" value={firstName} onChange={(e) => setFirstName(e.target.value)} />
                </div>
                <div>
                    <label htmlFor="lname">Last Name</label>
                    <input type="text" id="lname" value={lastName} onChange={(e) => setLastName(e.target.value)} />
                </div>
                <div>
                    <label htmlFor="dob">Date of Birth</label>
                    <input type="date" id="dob" value={dob} onChange={(e) => setDOB(e.target.value)} />
                </div>
                <div>
                    <label htmlFor="gender">Gender</label>
                    <select id="gender" value={gender} onChange={(e) => setGender(e.target.value)}>
                        <option value="">Select Gender</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div>
                    <label htmlFor="signupEmail">Email Address</label>
                    <input type="email" id="signupEmail" value={email} onChange={(e) => setEmail(e.target.value)} />
                </div>
                <div>
                    <label htmlFor="signupPassword">Password</label>
                    <input type="password" id="signupPassword" value={password} onChange={(e) => setPassword(e.target.value)} />
                </div>
                <div>
                    <label htmlFor="confirmPassword">Confirm Password</label>
                    <input type="password" id="confirmPassword" value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)} />
                </div>
                <div>
                    <button type="submit">Signup</button>
                </div>
                <div className="link-prompt">
                    Already have an account? <Link to="/login">Login here.</Link>
                </div>
            </form>
        </div>
    );
};

export default Register;
