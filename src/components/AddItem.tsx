import React, { useState, useEffect } from 'react';
import axios from 'axios';
import '../assets/css/AddItem.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';


interface OpenLocations {
    [key: string]: boolean;
}

interface FoodItem {
    foodId: number;
    foodName: string;
    foodCalorie: number;
    foodFat: number;
    foodCholesterol: number;
    foodSodium: number;
    foodCarb: number;
    foodSugar: number;
    foodProtein: number;
    foodServingTime: string;
    foodLocation: string;
    quantity?: number; // Added for managing quantity in the UI
}

interface AddItemProps {
    mealDate: Date;
    onAddItem: (item: FoodItem, date: Date) => void;
    onClose: (item: any) => void;
}

const AddItem: React.FC<AddItemProps> = ({ mealDate, onAddItem, onClose }) => {
    const [foodItems, setFoodItems] = useState([]);
    const [openLocations, setOpenLocations] = useState<OpenLocations>({});

    useEffect(() => {
        fetchMealData();
        console.log('mealDate:', mealDate);
    }, [mealDate]);

    const fetchMealData = async () => {
        const year = mealDate.getFullYear();
        const month = String(mealDate.getMonth() + 1).padStart(2, '0');
        const day = String(mealDate.getDate()).padStart(2, '0');

        const formattedDate = `${year}${month}${day}`;
        const url = `https://backend-gobbleup.discovery.cs.vt.edu/meals/food/`;
        const config = {
            headers: {
                'Content-Type': 'application/json',
            },
            params: {
                date: formattedDate,
            }
        };

        try {
            const response = await axios.get(url, config);
            console.log('Fetched data:', response.data.food_items);
            const mealData = response.data.food_items.map((item: any) => ({
                ...item,
                quantity: 1, // Default quantity
            }));
            setFoodItems(mealData);
        } catch (error) {
            console.error('Error fetching meal data:', error);
        }
    };

    const handleToggleLocation = (location: string) => {
        setOpenLocations(prev => ({
            ...prev,
            [location]: !prev[location]
        }));
    };

    const groupedByLocation = foodItems.reduce<{ [key: string]: FoodItem[] }>((acc, item) => {
        const { foodLocation } = item;
        if (!acc[foodLocation]) {
            acc[foodLocation] = [];
        }
        acc[foodLocation].push(item);
        return acc;
    }, {});

    return (
        <div className="add-item-overlay">
            <div className="add-item">
                <button className="close-button" onClick={onClose}>
                    <FontAwesomeIcon icon={faTimes}/>
                </button>
                <div className="add-buttons">
                {Object.entries(groupedByLocation).map(([location, items]) => (
                    <div key={location}>
                        <button onClick={() => handleToggleLocation(location)}>
                            {location} {openLocations[location] ? '▲' : '▼'}
                        </button>
                        {openLocations[location] && (
                            <table>
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Calories</th>
                                    <th>Add</th>
                                </tr>
                                </thead>
                                <tbody>
                                {items.map(item => (
                                    <tr key={item.foodId}>
                                        <td>{item.foodName}</td>
                                        <td>{item.foodCalorie}</td>
                                        <td>
                                            <button onClick={() => onAddItem(item, mealDate)}>Add</button>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        )}
                    </div>
                ))}
                </div>
            </div>
        </div>
    );
};

export default AddItem;
