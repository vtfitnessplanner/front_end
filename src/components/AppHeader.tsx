import {Link} from 'react-router-dom';
import '../assets/css/global.css';
import '../assets/css/AppHeader.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUser} from '@fortawesome/free-solid-svg-icons';
import React from "react";
import {useAuth} from "../Contexts/AuthContext";

function AppHeader() {

    const { isLoggedIn } = useAuth();

    return (
        <header className="container">
            <section className="gobbleup-logo">
                <Link to="/">
                    <img
                        src={require('../assets/images/gobbleup_logo.png')}
                        alt="GobbleUp Logo"
                        width="100px"
                        height="auto"
                    />
                </Link>
            </section>
            <h1 className="text-logo">
                <Link to="/" className="text-logo">GobbleUp</Link>
            </h1>
            <section className="login-user">
                {isLoggedIn ? (
                    <Link to="/profile">
                        <FontAwesomeIcon icon={faUser}/>
                    </Link>
                ) : (
                    <Link to="/login">
                        <button className="button">Log In</button>
                    </Link>
                )}
            </section>
        </header>
    );
}

export default AppHeader;

