import React, {useState, useEffect} from 'react';
import '../assets/css/Meal.css';
import AddItem from "./AddItem";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

interface FoodItem {
    mealId?: number;
    foodId: number;
    foodName: string;
    foodCalorie: number;
    foodFat: number;
    foodCholesterol: number;
    foodSodium: number;
    foodCarb: number;
    foodSugar: number;
    foodProtein: number;
    foodServingTime: string;
    foodLocation: string;
    quantity?: number; // Added for managing quantity in the UI
}

interface MealProps {
    mealName: string;
    mealDate: Date;
    items: FoodItem[];
    onClose: () => void;
    onAddItem: (item: FoodItem, date: Date) => void;
    onItemQuantityChange: (index: number, change: number) => void;
    onRemoveItem: (item: FoodItem) => void;
}

// @ts-ignore
const Meal = ({ mealName, mealDate, items, onClose, mealHistory, onAddItem, onRemoveItem }) => {
    // @ts-ignore
    const filteredItems: FoodItem[] = mealHistory.filter((item): item is FoodItem => item.foodServingTime === mealName);
    const [foodItems, setFoodItems] = useState<FoodItem[]>([]);
    const [showAddItem, setShowAddItem] = useState(false);


    const handleShowAddItem = () => {
        setShowAddItem(true); // This will be used to toggle the AddItem component
    };


    const handleCloseAddItem = () => {
        setShowAddItem(false);
    };

    const handleOnRemoveItem = (item: FoodItem) => {
        onRemoveItem(item);
    }

    return (
        <div className="meal-overlay">
            <div className="meal-modal">
                <button className="close-button" onClick={onClose}>
                    <FontAwesomeIcon icon={faTimes}/>
                </button>
                <h2>{mealName}</h2>

                {/* Start of Table Container */}
                <div className="table-container">
                    {filteredItems.length > 0 ? (
                        <table>
                            <tbody>
                            {filteredItems.map((food, index) => (
                                <tr key={food.foodId}>
                                    <td>{food.foodName}</td>
                                    <td>{food.foodCalorie} cals</td>
                                    <td>{food.foodProtein}g</td>
                                    <td>{food.foodCarb}g</td>
                                    <td>{food.foodFat}g</td>
                                    <td>{food.foodSugar}g</td>
                                    <td>
                                        <FontAwesomeIcon
                                            icon={faTimes}
                                            onClick={() => handleOnRemoveItem(food)}
                                            className="remove-item-icon"
                                        />
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    ) : (
                        <p>No items available for {mealName}.</p>
                    )}
                    <button className="add-item-button" onClick={handleShowAddItem}>
                        Add Item
                    </button>
                </div>
                {/* End of Table Container */}

                {showAddItem && (
                    <div className="add-item-overlay">
                        <AddItem
                            mealDate={mealDate}
                            onAddItem={(item, date) => {
                                onAddItem(item, date);
                                handleCloseAddItem(); // Close the AddItem interface after item is added
                            }}
                            onClose={handleCloseAddItem} // Pass the close function as a prop
                        />
                        {/* No need for the close button here, AddItem has its own */}
                    </div>
                )}
            </div>
        </div>
    );
};


export default Meal;
