import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import '../assets/css/global.css';
import '../assets/css/Home.css';

function Home() {
    const navigate = useNavigate();

    useEffect(() => {
        const isLoggedIn = localStorage.getItem('isLoggedIn');
        if (isLoggedIn !== 'true' || isLoggedIn === null) {
            navigate('/login');
        }
        else {
            navigate('/dashboard')
        }
    }, [navigate]);

    return (
        <div className="home-pages">
            <div className="welcome-text flow-content container dark-background">
            </div>
        </div>
    );
}

export default Home;
