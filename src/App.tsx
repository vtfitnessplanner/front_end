import React from 'react';
import {
    BrowserRouter as Router,
    Routes,
    Route,
} from "react-router-dom";
import AppHeader from "./components/AppHeader"
import AppFooter from "./components/AppFooter"
import Home from "./components/Home"
import Register from "./components/Register";
import Login from "./components/Login";
import Profile from "./components/Profile";
import Dashboard from "./components/Dashboard";

function App() {
    return (
        <Router basename={""}>
            <AppHeader />
            <div className="app">
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path={"/register"} element={<Register />} />
                    <Route path={"/login"} element={<Login />} />
                    <Route path={"/profile"} element={<Profile />} />
                    <Route path={"/dashboard"} element={<Dashboard />} />
                    <Route path="*" element={<div>Page Not Found</div>} />
                </Routes>
            </div>
            <AppFooter />
        </Router>
  );
}

export default App;
